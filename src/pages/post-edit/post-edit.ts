import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { JsonProvider } from '../../providers/json/json';

/**
 * Generated class for the PostEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-edit',
  templateUrl: 'post-edit.html',
})
export class PostEditPage {
  model: Post;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private proveedor: JsonProvider) {
    if (this.navParams.data.post){
      this.model = this.navParams.data.post;
    } else {
      this.model = new Post();
    }
  }

  save(){
    this.savePost()
      .then(() => {
        this.toast.create({ message: 'Post editado con exito!', position: 'botton', duration: 3000}).present();
        this.navCtrl.pop();
      })
      .catch((error) => {
        this.toast.create({ message: 'Error al editar, Error: ' +error.error, position: 'botton', duration: 3000}).present();
      })
  }

  private savePost(){
    if(this.model.id) {
      return this.proveedor.editarPost(this.model);
    } else {
      return this.proveedor.insert(this.model);
    }
  }

}

export class Post {
  id: number;
  title: string;
  body: string;
}
