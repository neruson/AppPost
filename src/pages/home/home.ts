import { Component } from '@angular/core';
import { NavController, NavParams, App, ViewController, ToastController } from 'ionic-angular';
import { JsonProvider } from '../../providers/json/json';
import { PostPage } from '../post/post';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  pushPage: any;
  posts: any;
  users: any;
  avatars: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, public proveedor: JsonProvider) {
    this.pushPage = PostPage;
  }

  ionViewDidEnter(){
    this.posts = [];
    this.users = [];
    this.avatars = [];
    this.getAllUsers();
    this.getAllPost();
    this.getAllImg();
    Object.keys(this.posts);
    
  }

  getAllPost() {
    this.proveedor.obtenerPost()
      .then((result: any) => {
        for (var i = 0; i < result.length; i++) {
          var post = result[i];
          this.posts.push(post);
        }
      })
      .catch((error: any) => {
        this.toast.create({ message: 'Error al listar los post. Error: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  getAllUsers() {
    this.proveedor.obtenerDatosUsers()
      .then((result: any) => {
        for (var i = 0; i < result.length; i++) {
          var user = result[i];
          this.users.push(user);
        }
      })
      .catch((error: any) => {
        this.toast.create({ message: 'Error al listar los post. Error: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  getAllImg() {
    this.proveedor.obtenerDatosAvatar()
      .then((result: any) => {
        for (var i = 0; i < result.length; i++) {
          var avatar = result[i];
          this.avatars.push(avatar);
        }
      })
      .catch((error: any) => {
        this.toast.create({ message: 'Error al listar los post. Error: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  openPost(id: number){
    this.proveedor.get(id)
      .then((result: any) => {
        this.navCtrl.push( 'PostDetailPage', { post: result });
      })
      .catch((error: any) => {
        this.toast.create({ message: 'Error al recuperar post, Error: ' + error.error, position: 'botton', duration: 3000}).present();
      })
  }

  openCreatePost(){
    this.navCtrl.push('PostEditPage');
  }

  openEditPost(id: number){
    this.proveedor.get(id)
      .then((result: any) => {
        this.navCtrl.push( 'PostEditPage', { post: result.data });
      })
      .catch((error: any) => {
        this.toast.create({ message: 'Error al recuperar post, Error: ' + error.error, position: 'botton', duration: 3000}).present();
      })
  }

  deletePost(post: any){
    this.proveedor.eliminarPost(post)
      .then((result: any) => {
        let index = this.posts.indexOf(post);
        this.posts.splice(index, 1);

        this.toast.create({ message: 'Post eliminado con exito.', position: 'botton', duration: 3000}).present();
      })
      .catch((error: any) => {
        this.toast.create({ message: 'Error eliminando post, Error: ' + error.error, position: 'button', duration: 3000 }).present();
      });
  }
}
