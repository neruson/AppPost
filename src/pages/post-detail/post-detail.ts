import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { JsonProvider } from '../../providers/json/json';

/**
 * Generated class for the PostDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-detail',
  templateUrl: 'post-detail.html',
})
export class PostDetailPage {
  post: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private proveedor: JsonProvider) {
    this.post = this.navParams.data.post
  }

  openCreatePost(){
    this.navCtrl.push('PostEditPage');
  }

  openEditPost(id: number){
    this.proveedor.get(id)
      .then((result: any) => {
        this.navCtrl.push( 'PostEditPage', { post: result.data });
      })
      .catch((error: any) => {
        this.toast.create({ message: 'Error al recuperar post, Error: ' + error.error, position: 'botton', duration: 3000}).present();
      })
  }

  deletePost(post: any){
    this.proveedor.eliminarPost(post)
      .then((result: any) => {
        let index = this.posts.indexOf(post);
        this.posts.splice(index, 1);

        this.toast.create({ message: 'Post eliminado con exito.', position: 'botton', duration: 3000}).present();
      })
      .catch((error: any) => {
        this.toast.create({ message: 'Error eliminando post, Error: ' + error.error, position: 'button', duration: 3000 }).present();
      });
  }

}