import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'user-contact',
  templateUrl: 'user.html'
})
export class UserPage {

  constructor(public navCtrl: NavController) {

  }

}
