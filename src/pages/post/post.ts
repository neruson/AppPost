import { Component } from '@angular/core';
import { JsonProvider } from '../../providers/json/json';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the PostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {
  model: post;

  constructor(public navCtrl: NavController, private toast: ToastController, private provider: JsonProvider, public navParams: NavParams) {
  this.model = new post();
  this.model.title = "Bienvenido"
  this.model.body = "Esto es una prueba"
  }

  createPost() {
    this.provider.subirPost( this.model.title, this.model.body)
      .then((result: any) => {
        this.toast.create({ message: 'Has subido un Post con exito. post: ' + result.id, position: 'botton', duration: 3000}).present();
        console.log(result.id);
      })
      .catch((error: any) =>{
        this.toast.create({ message: 'Error al enviar el post, error: ' + error.error, position: 'botton', duration: 3000}).present();
      });
  }

}

export class post{
  userId: number;
  title: string;
  body: string;
}