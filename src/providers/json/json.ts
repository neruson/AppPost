import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { NgModuleCompileResult } from '@angular/compiler/src/ng_module_compiler';

@Injectable()
export class JsonProvider {

  API_url = 'http://jsonplaceholder.typicode.com/';

  constructor(public http: HttpClient) { }

  obtenerDatosUsers(){
    return new Promise((resolve, reject) => {
      let url = this.API_url + 'users';
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
        },
      (error) => {
        reject(error);
      });
    });
  }

  obtenerDatosAvatar(){
    return new Promise((resolve, reject) => {
      let url = this.API_url + 'photos';
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
        },
      (error) => {
        reject(error);
      });
    });
  }

  obtenerPost(){
    return new Promise((resolve, reject) => {
      let url = this.API_url + 'posts';
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
        },
      (error) => {
        reject(error);
      });
    });
  }

  get(id: number){
    return new Promise((resolve, reject) => {
      let url = this.API_url + 'posts/' + id;

      this.http.get(url)
        .subscribe((result:any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  insert(post:any){
    return new Promise((resolve, reject) => {
      let url = this.API_url + 'posts/';
      this.http.post(url, post)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
      });
    });
  }

  subirPost(title:any, body:any){
    return new Promise((resolve, reject) => {
      let url = this.API_url + 'posts';
      var data = {
        title: title,
        body: body
      }

      this.http.post(url,data)
        .subscribe((result: any) => {
          resolve(result);
        },
      (error) => {
        reject(error);
      });
    });
  }

  editarPost(post: any){
    return new Promise((resolve, reject) => {
      let url = this.API_url + 'posts/' + post.id;
      var data = {
        "title": post.title,
        "body": post.body
      }

      this.http.post(url,data)
        .subscribe((result: any) => {
          resolve(result);
        },
      (error) => {
        reject(error);
      });
    });
  }

  eliminarPost(post: number){
    return new Promise((resolve, reject) => {
      let url = this.API_url + 'posts/' + post;

      this.http.delete(url)
        .subscribe((result: any) => {
          resolve(result);
        },
      (error) => {
        reject(error);
      });
    });
  }

}
